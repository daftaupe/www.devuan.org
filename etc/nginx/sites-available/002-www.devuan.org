server {
	listen 80;
#	listen [::]:80;

  	server_name www.devuan.org dev-1.org dev-one.org devuan.org;
		
	location ~ ^/.well-known/acme-challenge {
	    root /var/www/devuan-www/letsencrypt;
  	}
#	try_files $uri $uri/index.html $uri.htm $uri.html;
	
	##root /var/www/devuan-www/public;

	location / {	
		return 301 https://www.devuan.org$request_uri;
	}


}

server {
	listen 80;
  	server_name devuanzuwu3xoqwp.onion;
	root /var/www/devuan-www/public.current;
	
        include /etc/nginx/snippets/devuan-web.conf;
}

server {

	listen 443 ssl;
#	listen [::]:443;

  	server_name www.devuan.org dev-1.org dev-one.org devuan.org;
	root /var/www/devuan-www/public.current;
	
        include /etc/nginx/snippets/devuan-web.conf;

	ssl on;
    ssl_certificate /etc/letsencrypt/live/dev-1.org/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/dev-1.org/privkey.pem; # managed by Certbot
	ssl_protocols          TLSv1.2 TLSv1.1 TLSv1;
	ssl_ciphers            "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_ecdh_curve secp384r1; 
	ssl_prefer_server_ciphers on;
	ssl_session_cache   builtin:1000 shared:SSL:10m;
	resolver 213.186.33.99 valid=300s;
	resolver_timeout 3s;
}
