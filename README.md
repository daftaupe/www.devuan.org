# Devuan Web Source

This is the source for Devuan's web site `www.devuan.org`

Development occurs on the `new-beta` branch, where only the `source/` directory
is used and automatically published verbatim to `beta.devuan.org`

# Preview

To preview your changes simply step into `source/` and launch
```
python3 -m http.server
```
then navigate to http://localhost:8000

Every change requires a refresh of the page.

The main site `www.devuan.org` is published by manual copy of `beta.devuan.org`
on the server, except for `sitemap.xml` which is generated with respect to
`https://beta.devuan.org`.

# Publish

The file `.PUBLISH` is a control file for publishing the current
`beta.devuan.org` onto `www.devuan.org`.

When you are ready to publish just update that file:
```
date -R >> .PUBLISH
```
then edit it and add a note of text to the right side of your entry.

When one adds, commits and pushes a change to `.PUBLISH` the automatic
publishing process will refresh `www.devuan.org` verbatim from
`beta.devuan.org`, except for `sitemap.xml` which is generated with respect to
`https://www.devuan.org`.

Thank you for contributing!

# Attic

All else found in this project is old stuff, especially the ruby bits.

## License

The source code is free software distributed under the GNU Affero General
Public License, version 3, or, at your option, any later version. (See
[COPYING](./COPYING).)

The website contents are creative commons released under
CC-BY-ND-4.0-International.

