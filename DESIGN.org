#+TITLE:       Design Document for Devuan.org
#+AUTHOR:      hellekin
#+AUTHOR:      golinux
#+EMAIL:       editors@devuan.org
#+LANGUAGE:    en

* Design Document
** Inspirations
** Principles
*** Compatibility

    The Web should work with only HTML5, and be usable and elegant in a
    text browser, such as ~lynx -dump~, ~w3m~, or using ~curl~.

*** Minimalism

    The Web should not make use of unnecessary components: stylesheets
    must bring something new (readability, aesthetics), JavaScript
    components must only add optional functionality.

** Visual Design
